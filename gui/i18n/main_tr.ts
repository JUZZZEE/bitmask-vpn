<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.1">
<context>
    <name>About</name>
    <message>
        <location filename="../components/About.qml" line="9"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../components/About.qml" line="80"/>
        <source>&lt;p&gt;This service is paid for entirely by donations from users like you. &lt;a href=&quot;%1&quot;&gt;Please donate&lt;/a&gt;.&lt;/p&gt;</source>
        <extracomment>donation text of the about dialog</extracomment>
        <translation>&lt;p&gt;Bu hizmetin bedeli, sizin gibi kullanıcıların bağışlarıyla sağlanıyor. &lt;a href=&quot;%1&quot;&gt;Lütfen bağış yapın&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../components/About.qml" line="102"/>
        <source>%1 version: 
%2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; version string</extracomment>
        <translation>%1 sürüm: 
%2</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <location filename="../components/Footer.qml" line="171"/>
        <source>Recommended</source>
        <translation>Önerilen</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../components/Help.qml" line="7"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location filename="../components/Help.qml" line="20"/>
        <source>Troubleshooting and support</source>
        <translation>Sorun giderme ve destek</translation>
    </message>
    <message>
        <location filename="../components/Help.qml" line="31"/>
        <source>Report a bug</source>
        <translation>Bir hata bildirin</translation>
    </message>
    <message>
        <location filename="../components/Help.qml" line="42"/>
        <source>How to uninstall</source>
        <translation>Nasıl kaldırılır</translation>
    </message>
</context>
<context>
    <name>InitErrors</name>
    <message>
        <location filename="../components/InitErrors.qml" line="31"/>
        <source>Could not find helpers. Please check your installation</source>
        <translation>Yardımcılar bulunamıyor, lütfen kurulumunuzu kontrol edin.</translation>
    </message>
    <message>
        <location filename="../components/InitErrors.qml" line="44"/>
        <source>Could not find polkit agent.</source>
        <translation>polkit vekili bulunamıyor.</translation>
    </message>
</context>
<context>
    <name>Locations</name>
    <message>
        <location filename="../components/Locations.qml" line="21"/>
        <source>Select Location</source>
        <translation>Konum Seçin</translation>
    </message>
    <message>
        <location filename="../components/Locations.qml" line="27"/>
        <source>Automatically use best connection</source>
        <extracomment>this is in the radio button for the auto selection</extracomment>
        <translation>Otomatik olarak en iyi bağlantıyı kullan</translation>
    </message>
    <message>
        <location filename="../components/Locations.qml" line="29"/>
        <source>Manually select</source>
        <extracomment>Location Selection: label for radio buttons that selects manually</extracomment>
        <translation>Elle seçin</translation>
    </message>
    <message>
        <location filename="../components/Locations.qml" line="31"/>
        <source>Switching gateway…</source>
        <extracomment>A little display to signal that the clicked gateway is being switched to</extracomment>
        <translation>Ağ geçitleri değiştiriliyor…</translation>
    </message>
    <message>
        <location filename="../components/Locations.qml" line="33"/>
        <source>Only locations with bridges</source>
        <extracomment>Subtitle to explain that only bridge locations are shown in the selector</extracomment>
        <translation>Salt köprü içeren konumlar</translation>
    </message>
    <message>
        <location filename="../components/Locations.qml" line="65"/>
        <source>Recommended</source>
        <extracomment>Location Selection: label for radio button that selects automatically</extracomment>
        <translation>Önerilen</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../components/MainView.qml" line="33"/>
        <location filename="../components/MainView.qml" line="53"/>
        <source>Donate</source>
        <translation>Bağış yapın</translation>
    </message>
    <message>
        <location filename="../components/MainView.qml" line="46"/>
        <source>Preferences</source>
        <translation>Tercihler</translation>
    </message>
    <message>
        <location filename="../components/MainView.qml" line="60"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location filename="../components/MainView.qml" line="68"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../components/MainView.qml" line="76"/>
        <source>Quit</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../components/MainView.qml" line="107"/>
        <source>Please donate!</source>
        <translation>Lütfen bağış yapın!</translation>
    </message>
    <message>
        <location filename="../components/MainView.qml" line="121"/>
        <source>This service is paid for entirely by donations from users like you. The cost of running the VPN is approximately 5 USD per person every month, but every little bit counts. Do you want to donate now?</source>
        <translation>Bu hizmet tamamen sizin gibi kullanıcıların bağışlarıyla yapılıyor. Bir VPN&apos;i çalıştırma maliyeti, kişi başına aylık olarak 5 dolar civarıdır, ancak her katkı değerlidir. Şimdi siz de bağış yapmak ister misiniz?</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../components/Preferences.qml" line="10"/>
        <source>Preferences</source>
        <translation>Tercihler</translation>
    </message>
    <message>
        <location filename="../components/Preferences.qml" line="38"/>
        <source>Turn off the VPN to make changes</source>
        <translation>Değişiklik yapmak için VPN&apos;i kapatın</translation>
    </message>
    <message>
        <location filename="../components/Preferences.qml" line="48"/>
        <source>Censorship circumvention</source>
        <translation>Sansür önleme</translation>
    </message>
    <message>
        <location filename="../components/Preferences.qml" line="56"/>
        <source>These techniques can bypass censorship, but are slower. Use them only when needed.</source>
        <translation>Bu yöntemler sansürü aşabilir, ancak yavaştır. Sadece gerektiğinde kullanın.</translation>
    </message>
    <message>
        <location filename="../components/Preferences.qml" line="70"/>
        <source>Use obfs4 bridges</source>
        <translation>obfs4 köprülerini kullan</translation>
    </message>
    <message>
        <location filename="../components/Preferences.qml" line="96"/>
        <source>Traffic is obfuscated to bypass blocks</source>
        <translation>Engelleri aşmak için trafik gizlenir</translation>
    </message>
    <message>
        <location filename="../components/Preferences.qml" line="109"/>
        <source>Use Snowflake</source>
        <translation>Snowflake Kullan</translation>
    </message>
    <message>
        <location filename="../components/Preferences.qml" line="124"/>
        <source>Snowflake needs Tor installed in your system</source>
        <translation>Snowflake sisteminizde Tor kurulumu gerektirir</translation>
    </message>
    <message>
        <location filename="../components/Preferences.qml" line="136"/>
        <source>Transport</source>
        <translation>Aktarım</translation>
    </message>
    <message>
        <location filename="../components/Preferences.qml" line="144"/>
        <source>UDP can make the VPN faster. It might be blocked on some networks.</source>
        <translation>UDP, VPN&apos;i daha hızlı yapabilir. Bazı ağlarda engellenmiş olabilir.</translation>
    </message>
    <message>
        <location filename="../components/Preferences.qml" line="157"/>
        <source>Use UDP if available</source>
        <translation>Mevcutsa UDP kullan</translation>
    </message>
</context>
<context>
    <name>Splash</name>
    <message>
        <location filename="../components/Splash.qml" line="100"/>
        <source>There is a newer version available. </source>
        <translation>Yeni bir sürüm mevcut.</translation>
    </message>
    <message>
        <location filename="../components/Splash.qml" line="100"/>
        <source>Make sure to &lt;a href=&quot;https://0xacab.org/leap/bitmask-vpn/-/blob/main/docs/uninstall.md&quot;&gt;uninstall&lt;/a&gt; the previous one before running the new installer.</source>
        <translation>Yeni kurulumu başlatmadan önce lütfen öncekini &lt;a href=&quot;https://0xacab.org/leap/bitmask-vpn/-/blob/main/docs/uninstall.md&quot;&gt;kaldırdığınızdan&lt;/a&gt; emin olun.</translation>
    </message>
    <message>
        <location filename="../components/Splash.qml" line="104"/>
        <source>UPGRADE NOW</source>
        <translation>ŞİMDİ YÜKSELT</translation>
    </message>
</context>
<context>
    <name>Systray</name>
    <message>
        <location filename="../components/Systray.qml" line="16"/>
        <source>Checking status…</source>
        <translation>Durum denetleniyor…</translation>
    </message>
    <message>
        <location filename="../components/Systray.qml" line="36"/>
        <source>Donate</source>
        <translation>Bağış yapın</translation>
    </message>
    <message>
        <location filename="../components/Systray.qml" line="45"/>
        <source>Hide</source>
        <extracomment>Part of the systray menu; show or hide the main app window</extracomment>
        <translation>Gizle</translation>
    </message>
    <message>
        <location filename="../components/Systray.qml" line="45"/>
        <source>Show</source>
        <translation>Göster</translation>
    </message>
    <message>
        <location filename="../components/Systray.qml" line="57"/>
        <source>Quit</source>
        <extracomment>Part of the systray menu; quits que application</extracomment>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../components/Systray.qml" line="72"/>
        <source>Connect to</source>
        <translation>Bağlan</translation>
    </message>
    <message>
        <location filename="../components/Systray.qml" line="74"/>
        <source>Connect</source>
        <translation>Bağlan</translation>
    </message>
    <message>
        <location filename="../components/Systray.qml" line="77"/>
        <source>Disconnect</source>
        <translation>Bağlantıyı kes</translation>
    </message>
</context>
<context>
    <name>VPNState</name>
    <message>
        <location filename="../components/VPNState.qml" line="26"/>
        <location filename="../components/VPNState.qml" line="125"/>
        <source>Connecting</source>
        <translation>Bağlanıyor</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="58"/>
        <source>Unsecured
Connection</source>
        <translation>Güvensiz
Bağlantı</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="71"/>
        <source>Turn on</source>
        <translation>Aç</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="91"/>
        <source>Secured
Connection</source>
        <translation>Güvenli
Bağlantı</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="104"/>
        <source>Turn off</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="139"/>
        <source>Cancel</source>
        <translation>Vazgeç</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="223"/>
        <source>%1 off</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 kapalı</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="223"/>
        <source>off</source>
        <translation>kapalı</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="226"/>
        <source>%1 on</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 açık</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="229"/>
        <source>Connecting to %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 uygulamasına bağlanılıyor</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="232"/>
        <source>Stopping %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 durduruluyor</translation>
    </message>
    <message>
        <location filename="../components/VPNState.qml" line="235"/>
        <source>%1 blocking internet</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 interneti engelliyor</translation>
    </message>
</context>
<context>
    <name>VPNSwitch</name>
    <message>
        <location filename="../qml/VPNSwitch.qml" line="10"/>
        <source/>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="80"/>
        <source>: a fast and secure VPN. Powered by Bitmask.</source>
        <translation>: hızlı ve güvenli bir VPN. Bitmask tarafından sunuluyor.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="87"/>
        <source>Do not show the systray icon (useful together with Gnome Shell extension, or to control VPN by other means).</source>
        <translation>Sistem çubuğu simgesini gösterme (Gnome Kabuk eklentisiyle birlikte veya VPN&apos;i başka şekillerde denetlemek için kullanışlıdır)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="94"/>
        <source>Enable Web API.</source>
        <translation>Web API&apos;yi etkinleştir</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="100"/>
        <source>Install helpers (Linux only, requires sudo).</source>
        <translation>Yardımcıları kur (sadece Linux, sudo gerekiyor)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="106"/>
        <source>Use obfs4 to obfuscate the traffic, if available in the provider.</source>
        <translation>Trafiği gizlemek için obfs4 kullan, sadece sağlayıcıda kullanılabiliyorsa</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="112"/>
        <source>Disable autostart for the next run.</source>
        <translation>Sonraki çalıştırma için otomatik başlatmayı devre dışı bırak</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="117"/>
        <source>Web API port (default: 8080)</source>
        <translation>Web API portu (varsayılan: 8080)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="119"/>
        <source>Start the VPN, either &apos;on&apos; or &apos;off&apos;.</source>
        <translation>VPN&apos;i başlat, &apos;açık&apos; veya &apos;kapalı&apos;.</translation>
    </message>
</context>
</TS>